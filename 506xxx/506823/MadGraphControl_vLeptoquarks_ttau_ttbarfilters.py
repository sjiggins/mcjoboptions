from MadGraphControl.MadGraphUtils import *
import os
import re
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment

THIS_DIR = (os.environ['JOBOPTSEARCHPATH']).split(":")[0]
job_option_name = [ f for f in os.listdir(THIS_DIR) if (f.startswith('mc') and f.endswith('.py'))][0]
print job_option_name

nevents=5500
mode=0
#set all couplings to 0 to be sure 
#note that the couplings are lambda*sqrt(BR), we use lambda=0.3 usually
couplingDir = {}
for q in ['u', 'c', 't', 'd', 's','b']:
    for l in ['E','M','T','VE','VM','VT']:
        cpStr = "gs"+q+l+"L"
        couplingDir[cpStr] = 0.0
    
    for l in ['E','M','T']:
        cpStr = "gs"+q+l+"R"
        couplingDir[cpStr] = 0.0

JOname = job_option_name  
     
#LQs in the model
# vul: pdg ID 1202, charge 5/3, labelled "down" in the JOs
lqType = ""
lqPDGID = 0
xkappa = 0
tkappa = 0
if "_vLQd_" in JOname:
    lqType = "vul"  
    lqPDGID = 1202
else:
    raise RuntimeError("Cannot find LQ type in JO name.")

if not lqType:
    raise RuntimeError("No LQ type set.")
if not lqPDGID:
    raise RuntimeError("No LQ PDG ID set.")

if "_k00_" in JOname:
    xkappa = 0.0
    tkappa = 0.0
elif "_k11_" in JOname:
    xkappa = -1.0
    tkappa = -1.0
elif "_k01_" in JOname:
    xkappa = 0.0
    tkappa = -1.0
elif "_k10_" in JOname:
    xkappa = -1.0
    tkappa = 0.0
else:
    raise RuntimeError("Cannot find xkappa and tkappa in JO name.")

matchesVlqMass = re.search("Mlq([0-9]+)\_", JOname)
print "matchesVlqMass", matchesVlqMass
if matchesVlqMass is None:
    raise RuntimeError("Cannot find vlq mass string in JO name.")     
else:
    lqmass = float(matchesVlqMass.group(1))

matchesgpMass = re.search("Mgp([0-9]+)\.py", JOname)
if matchesgpMass is None:
    raise RuntimeError("Cannot find gp mass string in JO name.")     
else:
    gpmass = float(matchesgpMass.group(1))

#print "lqmass", lqmass, "gpmass", gpmass

evgenConfig.description = ('Pair production of vector leptoquarks, {0}, mLQ={1:d}, mpg={2:d}').format(lqType,int(lqmass),int(gpmass))

decays = []
JOlist = JOname.split("_")
for cp in couplingDir.keys():
    if cp in JOlist:
        idx = JOlist.index(cp)
        cpVal = JOlist[idx+1]
        cpVal = cpVal.replace('p','.')
        couplingDir[cp] = float(cpVal)
        quark = cp[2]
        lepton = ''
        if len(cp)==5:
            lepton = cp[3]
            if lepton=='T':
                lepton = lepton+'A'
            if lepton=='M':
                lepton = lepton+'U'
        elif len(cp)==6:
            lepton = cp[3]+cp[4]
        else:
            raise RuntimeError("Unexpected length of coupling sting.")
        decays.append([quark.lower(),lepton.lower()])
 
#assign correct decays
decayLines = []

#set top decay mode if specified
top_flg=False
for dec in decays:
    if dec[0] in ['t']:
        top_flg=True
tdecay = ""
tbardecay = ""
nLeptons = -99
if "_dil" in JOname:
    if not top_flg:
        raise RuntimeError("Top decay mode should not be used without top quark in the final state.")
    decayLines.append("define lv = e+ mu+ ta+ ve vm vt e- mu- ta- ve~ vm~ vt~")
    tdecay = ", (t > w+ b, w+ > lv lv)"
    tbardecay = ", (t~ > w- b~, w- > lv lv)"
    evgenConfig.description += ', dilepton'
elif "_allhad" in JOname:
    if not top_flg:
        raise RuntimeError("Top decay mode should not be used without top quark in the final state.")
    decayLines.append("define j = g u c d s b u~ c~ d~ s~ b~")
    tdecay = ", (t > w+ b, w+ > j j)"
    tbardecay = ", (t~ > w- b~, w- > j j)"
    evgenConfig.description += ', allhad'
elif "_nallhad" in JOname:
    if not top_flg:
        raise RuntimeError("Top decay mode should not be used without top quark in the final state.")
    #decayLines.append("define all = g u c d s b u~ c~ d~ s~ b~ e+ mu+ ta+ ve vm vt e- mu- ta- ve~ vm~ vt~")
    tdecay = ", (t > w+ b, w+ > all all)"
    tbardecay = ", (t~ > w- b~, w- > all all)"
    nLeptons = -1
    nevents = nevents*2.0
    evgenConfig.description += ', non-allhad'
elif "_incl" in JOname:
    if not top_flg:
        raise RuntimeError("Top decay mode should not be used without top quark in the final state.")
    tdecay = ", (t > w+ b, w+ > all all)"
    tbardecay = ", (t~ > w- b~, w- > all all)"
    evgenConfig.description += ', inclusive'
else:
    if top_flg:
        evgenConfig.description += ', inclusive'       
        
for dec in decays:      
      #charge 5/3     
     if lqPDGID==1202:
         if dec[0] in ['d','s','b']:
             if dec[1] in ['e','mu','ta']:
                 raise RuntimeError("Fermion charges cannot add up to LQ charge.")
             decayStatement = "decay " +  lqType + " > " + dec[0] + " " + dec[1]
             decayLines.append(decayStatement)
             decayStatement = "decay " +  lqType+"~ > " + dec[0]+"~ " + dec[1]+"~"
             decayLines.append(decayStatement)
         elif dec[0] in ['u','c','t']:
             if 'v' in dec[1]:
                 raise RuntimeError("Fermion charges cannot add up to LQ charge.")
             if dec[0] in ['t']:
                 decayStatement = "decay " +  lqType + " > " + dec[0] + " " + dec[1]+"+"+tdecay
                 decayLines.append(decayStatement)
                 decayStatement = "decay " +  lqType+"~ > " + dec[0]+"~ " + dec[1]+"-"+tbardecay
                 decayLines.append(decayStatement)
             else:
                 decayStatement = "decay " +  lqType + " > " + dec[0] + " " + dec[1]+"+"
                 decayLines.append(decayStatement)
                 decayStatement = "decay " +  lqType+"~ > " + dec[0]+"~ " + dec[1]+"-"
                 decayLines.append(decayStatement)
         else:
             raise RuntimeError("Unexpected quark flavour.")


fcard = open('proc_card_mg5.dat','w')
fcard.write("""
set group_subprocesses Auto
set ignore_six_quark_processes False
set loop_color_flows False
set gauge unitary
set complex_mass_scheme False
set max_npoint_for_channel 0
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define p = p b b~
define j = j b b~
import model vleptoquark_tta_UFO\n""")

#fcard.write("""generate p p > {0} {0}~ [all=QCD]
#fcard.write("""generate p p > {0} {0}~ [all]
fcard.write("""generate p p > {0} {0}~
output -f""".format(lqType))
fcard.close()



beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")



#process_dir = new_process()
process_dir = new_process(process='import model vleptoquark_tta_UFO\ngenerate p p > {0} {0}~\noutput -f'.format(lqType))


#Fetch default NLO run_card.dat and set parameters
#PDF sets: NNPDF30_nlo_as_0118, _as_0115, _as_0117, _as_0119, _as_121, MMHT2014nlo68cl, CT14nlo , , 264000, 265000, 266000, 267000, 25100, 13100
#extras = { 'pdlabel'       :"'lhapdf'",
#           #'lhaid'         :"260000 264000 265000 266000 267000 25100 13100",
#           'lhaid'         :"230000",
#           'parton_shower' :'PYTHIA8',
#           'reweight_scale':'True',
#           #'reweight_PDF':'True False False False False False False',
#           'reweight_PDF':'True',
##           'jetalgo':'-1.0',
#           'jetradius':'0.4',
#           'muR_ref_fixed' : lqmass,
#           'muF_ref_fixed' : lqmass,
#           'QES_ref_fixed' : lqmass}

settings = {
    ####'pdlabel'      :'lhapdf',
    #'pdlabel'      :'nn23lo1',
    #'lhaid'        :'260800',
    'scale'        :lqmass,
    'dsqrt_q2fact1':lqmass,
    'dsqrt_q2fact2':lqmass,
    'fixed_ren_scale' : True,
    'fixed_fac_scale' : True,
    
    'nevents' : 1.1*evgenConfig.nEventsPerJob,

    'lhe_version': 3.0,
    'clusinfo': True,
    'ickkw': 0,
    'alpsfact': 1.00,
    'chcluster': False,
    'asrwgtflavor': 4,
    'auto_ptj_mjj': False,
    'xqcut': 0.000000, 
    'ptgmin': 0.0,
    'R0gamma': 0.4,
    'xn': 1.0,
    'epsgamma': 1.0,
    'isoEM': True,
    'ktdurham': -1.0,
    'dparameter': 0.4,
    'ptlund': -1.0,
    'pdgs_for_merging_cut': "1 2 3 4 5 6 21 1202 1210", 
#####    'QES_ref_fixed': lqmass,
####    'mmll'         : 30,
}


#MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING=  {
#     'central_pdf':260000, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets 
#     'pdf_variations':[260000], # pdfs for which all variations (error sets) will be included as weights 
#     'alternative_pdfs':[90400,266000,265000,13100,25200], # pdfs for which only the central set will be included as weights 
#     'scale_variations':[0.5,1.,2.], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated 
#     'use_syst': True,}


modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=settings)


# Parameters
parameters = {
    'MASS':{
        '1202' : '{:e}'.format(lqmass),
        '1210' : '{:e}'.format(gpmass)
    },
    'FRBLOCK' :{
        #'1'   :  '1.000000e+00 # Ylq', 
        '1'   :  '3.000000e-01 # Ylq', 
        '2'   :  '{:e}'.format(tkappa),
        '3'   :  '{:e}'.format(xkappa)
    },
    'DECAY':{
        '1202' : 'Auto',
        '1210' : 'Auto'
    }
}

modify_param_card(process_dir=process_dir, params=parameters)

#madspin_card_loc='madspin_card.dat'                                                                                                                                    

#mscard = open(madspin_card_loc,'w')                                                                                                                                    
madspin_card=process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
# set BW_cut 15                # cut on how far the particle can be off-shell
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
# specify the decay for the final state particles
decay t > w+ b, w+ > all all
decay t~ > w- b~, w- > all all
decay w+ > all all
decay w- > all all
decay z > all all\n"""%runArgs.randomSeed)

for l in decayLines:
    mscard.write(l+'\n')

mscard.write("""# running the actual code
launch""")    
mscard.close()

pdgfile = open("pdgid_extras.txt", "w+")
pdgfile.write("""1202
-1202
1210
-1210
""")
pdgfile.close()

#testSeq.TestHepMC.UnknownPDGIDFile = '/home/mahsana/MCgen_BostonModel/Boston/joboptions/CommonFiles/pdgid_extras.txt'
testSeq.TestHepMC.UnknownPDGIDFile = 'pdgid_extras.txt'

#print_cards()
print_cards(run_card='run_card.dat', param_card='param_card.dat', madspin_card='madspin_card.dat') 
      
runName='run_01'     

generate(process_dir=process_dir, runArgs=runArgs)

arrange_output(process_dir=process_dir, lhe_version=3, saveProcDir=True, runArgs=runArgs)


#### Shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")


# --------------------------------------------------------------
# Apply TTbarWToLeptonFilter
# --------------------------------------------------------------
# Only truely needed for non-all hadronic decays
if nLeptons != -99:
  include("GeneratorFilters/TTbarWToLeptonFilter.py")
  filtSeq.TTbarWToLeptonFilter.NumLeptons = nLeptons
  filtSeq.TTbarWToLeptonFilter.Ptcut      = 0.0


evgenConfig.description = ('Pair production of U1 tilde vector leptoquarks, {0}, mLQ={1:d}, mpg={2:d}').format(lqType,int(lqmass),int(gpmass))
evgenConfig.keywords+=['BSM','exotic','leptoquark']
#evgenConfig.generators += ["aMcAtNlo", "Pythia8", "EvtGen"]
evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.process = 'pp -> vLQ vLQ'
evgenConfig.contact = ["Mahsana Haleem <mahsana.haleem@cern.ch>"]
#runArgs.inputGeneratorFile=outputDS


