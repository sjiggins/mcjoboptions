evgenConfig.description = "Inclusive pp->J/psi(2S)(mu6muX) production with Photos"
evgenConfig.process = "J/Psi(2S) -> 2mu"
evgenConfig.keywords = ["charmonium","2muon","inclusive"]
evgenConfig.contact  = ["sabidi@cern.ch"]
evgenConfig.nEventsPerJob = 500

include('Pythia8B_i/Pythia8B_A14_CTEQ6L1_Common.py')
include('Pythia8B_i/Pythia8B_Photospp.py')
include("Pythia8B_i/Pythia8B_Charmonium_Common.py")

genSeq.Pythia8B.Commands += ['9940103:m0 = 4.0']
genSeq.Pythia8B.Commands += ['9940103:mMin = 4.0']
genSeq.Pythia8B.Commands += ['9940103:mMax = 4.0']
genSeq.Pythia8B.Commands += ['9940103:0:products = 100443 21']

genSeq.Pythia8B.Commands += ['9941103:m0 = 4.0']
genSeq.Pythia8B.Commands += ['9941103:mMin = 4.0']
genSeq.Pythia8B.Commands += ['9941103:mMax = 4.0']
genSeq.Pythia8B.Commands += ['9941103:0:products = 100443 21']

genSeq.Pythia8B.Commands += ['9942103:m0 = 4.0']
genSeq.Pythia8B.Commands += ['9942103:mMin = 4.0']
genSeq.Pythia8B.Commands += ['9942103:mMax = 4.0']
genSeq.Pythia8B.Commands += ['9942103:0:products = 100443 21']

genSeq.Pythia8B.Commands += ['100443:onMode = off']
genSeq.Pythia8B.Commands += ['100443:1:onMode = on']
genSeq.Pythia8B.Commands += ['100443:2:onMode = on']
genSeq.Pythia8B.SignalPDGCodes = [100443,-13,13]



genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [6.0]
genSeq.Pythia8B.TriggerStateEtaCut = 2.5
genSeq.Pythia8B.MinimumCountPerCut = [1]

