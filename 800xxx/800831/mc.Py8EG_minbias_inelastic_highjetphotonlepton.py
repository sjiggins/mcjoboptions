evgenConfig.description = "High-pT Pythia8 inelastic minimum bias events for pileup, with the A3 NNPDF23LO tune and EvtGen, using high-pT jet, photon and B hadron and lepton filters."
evgenConfig.keywords = ["QCD", "minBias", "SM"]
evgenConfig.contact  = [ "jeff.dandoy@cern.ch", "jan.kretzschmar@cern.ch", "judith.katzy@cern.ch"]
evgenConfig.nEventsPerJob = 500

evgenConfig.saveJets = True
evgenConfig.savePileupTruthParticles = True

include("Pythia8_i/Pythia8_Base_Fragment.py")

genSeq.Pythia8.Commands += [
    "Tune:ee = 7",
    "Tune:pp = 14",
    "PDF:pSet=LHAPDF6:NNPDF23_lo_as_0130_qed",
    "MultipartonInteractions:bProfile = 2",
    "MultipartonInteractions:pT0Ref = 2.45",
    "MultipartonInteractions:ecmPow = 0.21",
    "MultipartonInteractions:coreRadius = 0.55",
    "MultipartonInteractions:coreFraction = 0.9",
    "SigmaDiffractive:PomFlux = 4",
    "SigmaDiffractive:PomFluxEpsilon = 0.07",
    "SigmaDiffractive:PomFluxAlphaPrime = 0.25",
    "ColourReconnection:range = 1.8",
    "SigmaElastic:rho = 0.0",
    "SigmaDiffractive:mode = 0",
    "SigmaTotal:sigmaXB = 6.416",
    "SigmaTotal:sigmaAX = 6.416",
    "SigmaTotal:sigmaXX = 8.798",
    "SigmaTotal:sigmaAXB = 0.",
    "SigmaDiffractive:OwnbMinDD = 5."
	]

evgenConfig.tune = "A3 NNPDF23LO"

include("Pythia8_i/Pythia8_EvtGen.py")

genSeq.Pythia8.Commands += [ "SoftQCD:inelastic = on" ]

include ("GeneratorFilters/AddPileupTruthParticles.py")
include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)
CreateJets(prefiltSeq, 0.6)

AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.6)
filtSeq.QCDTruthJetFilter.MinPt = 35.*GeV

include("GeneratorFilters/DirectPhotonFilter.py")
filtSeq.DirectPhotonFilter.NPhotons = 1
filtSeq.DirectPhotonFilter.Ptmin = [ 8000. ]
filtSeq.DirectPhotonFilter.Etacut = 4.5

## select events with a B hadron with a pT cut of 5 GeV. It is not required that the hadrons are within a jet.
include("GeneratorFilters/BHadronFilter.py")
filtSeq += HeavyFlavorBHadronFilter
filtSeq.HeavyFlavorBHadronFilter.BottomPtMin=5000.
filtSeq.HeavyFlavorBHadronFilter.BottomEtaMax=4.5

## select events with a leading lepton pT above 4 GeV and |eta|<4.5. Only electrons and muons are considered as leptons
include("GeneratorFilters/LeptonFilter.py")
filtSeq.LeptonFilter.Ptcut = 4.*GeV
filtSeq.LeptonFilter.Etacut = 4.5

filtSeq.Expression = 'DirectPhotonFilter or QCDTruthJetFilter or (HeavyFlavorBHadronFilter and LeptonFilter)'
