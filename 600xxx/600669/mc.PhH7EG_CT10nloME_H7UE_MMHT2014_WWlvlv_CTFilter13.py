#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig7 WW_lvlv production with H7UE MMHT2014 LO tune'
evgenConfig.keywords    = [ 'electroweak', 'diboson', 'WW', '2lepton', 'neutrino' ]
evgenConfig.contact     = [ 'jan.kuechler@cern.ch' ]
evgenConfig.tune        = "H7.1-Default"
evgenConfig.generators  += ["Powheg", "Herwig7", "EvtGen"]
evgenConfig.nEventsPerJob = 2000


#--------------------------------------------------------------
# Powheg WW setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_WW_Common.py')
PowhegConfig.decay_mode =  "w+ w- > l+ vl l'- vl'~" # --> translates to 'WWlvlv', see https://gitlab.cern.ch/atlas/athena/-/blob/21.6/Generators/PowhegControl/python/processes/powheg/WW.py#L182
PowhegConfig.withdamp = 1
PowhegConfig.bornzerodamp = 1
PowhegConfig.PDF = range( 11000, 11053 )+[ 21100, 260000 ] # CT10nlo 0-52, MSTW2008nlo68cl, NNPDF3.0
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 1.0, 1.0, 2.0, 2.0, 2.0 ]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 2.0, 0.5, 1.0, 2.0 ]
PowhegConfig.nEvents *= 7.0     # Dependent on filter efficiency
PowhegConfig.generate()

#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")
# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="CT10nlo")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")
# run Herwig7
Herwig7Config.run()


from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter

MultiLeptonFilter = MultiLeptonFilter("MultiLeptonFilter")
MultiLeptonFilter.Ptcut = 3000.
MultiLeptonFilter.Etacut = 4.5
MultiLeptonFilter.NLeptons = 2

from GeneratorFilters.GeneratorFiltersConf import ChargedTracksFilter
chtrkfilter = ChargedTracksFilter("ChargedTracksFilter")
chtrkfilter.NTracks=-1
chtrkfilter.NTracksMax=13
chtrkfilter.Ptcut=900.
chtrkfilter.Etacut=4.5

filtSeq += chtrkfilter
