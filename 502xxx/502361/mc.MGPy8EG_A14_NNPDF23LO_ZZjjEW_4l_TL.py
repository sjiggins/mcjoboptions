evgenConfig.nEventsPerJob = 2000

# ZZ inclusive or ZZjj_EW
isZZjj = True
# polarization states: TT, TL, LL
polStat = "TL"
# number of jets (used for ZZ QCD only)
njets = -2

include("MadGraphControl_Pythia8EvtGen_ZZ2jets_pol.py")
