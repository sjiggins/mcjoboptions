# Job options for vector leptoquark (U1) pair production
# To be used for MC16 in r21.6 (tested with 21.6.45)

from MadGraphControl.MadGraphUtils import *
import os
import re

# maxEvents given when running Gen_tf.py
nevents=runArgs.maxEvents

THIS_DIR = (os.environ['JOBOPTSEARCHPATH']).split(":")[0]
job_option_name = [ f for f in os.listdir(THIS_DIR) if (f.startswith('mc') and f.endswith('.py'))][0]
print job_option_name

matchesMass = re.search("M([0-9]+).*\.py", job_option_name)
if matchesMass is None:
    raise RuntimeError("Cannot find mass string in job option name: {:s}.".format(job_option_name))
else:
    lqmass = float(matchesMass.group(1))

gpmass = 1000
nb_events = nevents

# Set couplings
decays = []
JOlist = job_option_name.split("_")

yte = 0.0
ytmu = 0.0
if "te" in JOlist:
    yte = eval( JOlist[ JOlist.index("te")+1 ].replace('p','.'))
if yte != 0:
    decays.extend(['te'])
if "tmu" in JOlist:
    ytmu = eval( JOlist[ JOlist.index("tmu")+1 ].replace('p','.'))
if ytmu != 0:
    decays.extend(['tmu'])

had = 0
if "had" in JOlist:
    had=1

tkappa = 0.0
xkappa = 0.0
if "YM" in JOlist:
    tkappa = 0.0
    xkappa = 0.0
elif "min" in JOlist:
    tkappa = 1.0
    xkappa = 1.0
if 'tk' in JOlist:
    tkappa = eval( JOlist[ JOlist.index("tk")+1 ].replace('p','.'))
if 'xk' in JOlist:
    xkappa = eval( JOlist[ JOlist.index("xk")+1 ].replace('p','.'))

# assign correct decays
decayLines = []
for dec in decays:
    if dec[1] == 'e':
        decayStatement = 'decay vul > '+dec[0]+' '+dec[1]+'+'
        if had == 1:
            decayStatement += ', (t > w+ b, w+ > j j)' 
        decayLines.append(decayStatement)
        decayStatement = 'decay vul~ > '+dec[0]+'~ '+dec[1]+'-'
        if had == 1:
            decayStatement += ', (t~ > w- b~, w- > j j)'
        decayLines.append(decayStatement)         
    elif dec[1] == 'm':
        decayStatement = 'decay vul > '+dec[0]+' '+dec[1]+dec[2]+'+'
        if had == 1:
            decayStatement += ', (t > w+ b, w+ > j j)'
        decayLines.append(decayStatement)
        decayStatement = 'decay vul~ > '+dec[0]+'~ '+dec[1]+dec[2]+'-'
        if had == 1:
            decayStatement += ', (t~ > w- b~, w- > j j)'
        decayLines.append(decayStatement)
    else:
        raise RuntimeError("Unexpected decay")

# ecmEnergy given when running Gen_tf.py
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process(process='import model vleptoquark_te_tmu_UFO\ngenerate p p > vul vul~\noutput -f')

# Fetch default NLO run_card.dat and set parameters
# 260800 corresponds to NNPDF30_nlo_as_0118_mc
settings = {
    'nevents'      :1.1*nevents,
    'pdlabel'      :'lhapdf',
    'lhaid'        :'260800',
    'scale'        :lqmass,
    'dsqrt_q2fact1':lqmass,
    'dsqrt_q2fact2':lqmass
}
modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=settings)

# Parameters
parameters = {
    'MASS':{
        '1202'    : '{:e}'.format(lqmass)
#        '1210'    : '{:e}'.format(gpmass)
    },
    'FRBLOCK':{
        '1'       : '{:e}'.format(yte),
        '2'       : '{:e}'.format(ytmu),
        '3'       : '{:e}'.format(tkappa),
        '4'       : '{:e}'.format(xkappa),
    },
    'DECAY':{
        '1202'    : 'Auto'
#        '1210'    : 'Auto'
    }
}
modify_param_card(process_dir=process_dir, params=parameters)

# MadSpin
madspin_card = process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card, 'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer * 
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#*    Manual:                                               *
#*    cp3.irmp.ucl.ac.be/projects/madgraph/wiki/MadSpin     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
set seed %i
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
# set BW_cut 15                 # cut on how far the particle can be off-shell
# set spinmode onshell          # Use one of the madspin special mode
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event

# specify the decay for the final state particles
decay t > w+ b, w+ > all all
decay t~ > w- b~, w- > all all
decay w+ > all all
decay w- > all all
decay z > all all\n"""%runArgs.randomSeed)

if had == 1:
    mscard.write("""define j = g u c d s b u~ c~ d~ s~ b~\n""")

for l in decayLines:
    mscard.write(l+'\n')

mscard.write("""
# running the actual code
launch""")
mscard.close()

pdgfile = open("pdgid_extras.txt", "w+")
pdgfile.write("""1202
-1202
1210
-1210
""")
pdgfile.close()

# Print cards on screen
print_cards(run_card='run_card.dat', param_card='param_card.dat', madspin_card='madspin_card.dat')

generate(process_dir=process_dir, runArgs=runArgs)

#outputDS = arrange_output(process_dir=process_dir, lhe_version=3, saveProcDir=True, runArgs=runArgs)
arrange_output(process_dir=process_dir, lhe_version=3, saveProcDir=True, runArgs=runArgs)

# Metadata
evgenConfig.description = ('Pair production of vector leptoquarks with te/tmu decay, mLQ={0:d}').format(int(lqmass))
evgenConfig.keywords+=['BSM','exotic','leptoquark','vector']
evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.process = 'pp -> vul vul~'
evgenConfig.contact = ["Julian Nickel <juliannn9@gmail.com>", "Vincent Wong <vwong@cern.ch>"]
#runArgs.inputGeneratorFile=outputDS

# Shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

#evt_multiplier = 8
