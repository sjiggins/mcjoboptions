evgenConfig.description = "POWHEG-BOX-RES/OpenLoops+Pythia8+EvtGen ttbb (4FS), mur=1/2*[mT(top)*mT(tbar)*mT(b)*mT(bbar)]**(1/4), muf=1/2*[mT(top)+mT(tbar)+mT(b)+mT(bbar)+mT(gluon)], semileptonic channel, hdamp=HT/2, bornzerodamp=5, dipole recoil, A14 NNPDF23 LO tune, decays with Powheg"
evgenConfig.keywords = [ 'SM', 'top', 'ttbar', 'bbbar', '1lepton']
evgenConfig.contact = ["lars.ferencz@cern.ch"]
evgenConfig.generators += [ 'Powheg' ]
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 10

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
# Reshower lhe files with dipole recoil settings
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
### Change pTdef value
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 4' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]
genSeq.Pythia8.Commands += [ 'SpaceShower:dipoleRecoil = on' ]
