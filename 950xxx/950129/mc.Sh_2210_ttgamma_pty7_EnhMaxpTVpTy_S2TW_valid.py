include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")
include("Sherpa_i/EW_scheme_sinthetaW_mZ.py")

evgenConfig.description = "Sherpa llgamma + 0,1j@NLO + 2,3j@LO with 7<pT_y using (alpha[mZ],mZ,sin2ThetaW) scheme."
evgenConfig.keywords = ["SM", "2tau", "photon", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.nEventsPerJob = 500

genSeq.Sherpa_i.RunCard="""
(run){
  % Reduction in negative weights
  NLO_CSS_PSMODE=1

  # HT prime scale 
  PP_RS_SCALE VAR{sqr(sqrt(H_T2)-PPerp(p[2])-PPerp(p[3])+MPerp(p[2]+p[3]))/4};

  % scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  % no EW corrections, but for consistency with Z+jets
  METS_BBAR_MODE=5

  % tags for process setup
  NJET:=3; LJET:=3,4; QCUT:=20;

  % me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  SOFT_SPIN_CORRELATIONS=1
}(run)

(processes){
  Process 93 93 -> 22 15 -15 93{NJET}
  Enhance_Observable VAR{log10(max(PPerp(p[2]+p[3]),PPerp(p[4])))}|1|2.7 {3,4,5,6} 
  Order (*,3); CKKW sqr(QCUT/E_CMS)
  NLO_QCD_Mode MC@NLO {LJET}
  ME_Generator Amegic {LJET}
  RS_ME_Generator Comix {LJET}
  Loop_Generator LOOPGEN {LJET}
  PSI_ItMin 20000 {4}
  Integration_Error 0.99 {4}
  PSI_ItMin 50000 {5,6}
  Integration_Error 0.99 {5,6}
  End process

}(processes)

(selector){
  PTNLO  22  7  E_CMS
  IsolationCut  22  0.1  2  0.10
  DeltaRNLO  22  90  0.1 1000.0
  Mass  90  90  10.0  E_CMS
}(selector)
"""

genSeq.Sherpa_i.NCores = 16
genSeq.Sherpa_i.OpenLoopsLibs = [ "pplla","ppllaj"]

#factor alpha(0)/alpha(mZ) for the photon, using Sherpa default values
#KFACTOR VAR{128.802/137.03599976};
genSeq.Sherpa_i.CrossSectionScaleFactor = 0.939913601
