evgenConfig.description = "Single neutrinos with fixed eta and E: purely for pile-up/lumi testing"
evgenConfig.keywords = ["singleParticle", "neutrino"]
evgenConfig.contact = ["chris.g@cern.ch"] 
evgenConfig.generators += ["ParticleGun"]
 
import ParticleGun as PG
genSeq += PG.ParticleGun()
genSeq.ParticleGun.sampler.pid = 12
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=50000, eta=0.0)

