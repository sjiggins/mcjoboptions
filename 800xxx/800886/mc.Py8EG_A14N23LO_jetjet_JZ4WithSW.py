include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

evgenConfig.description = "Dijet truth jet slice JZ4, with the A14 NNPDF23 LO tune"
evgenConfig.keywords = ["QCD", "jets", "SM"]
evgenConfig.contact = ["simone.amoroso@cern.ch","alex.emerman@cern.ch"]
evgenConfig.nEventsPerJob = 10000

genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:Bias2Selection=on",
                            "PhaseSpace:pTHatMin = 150."]

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)    
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.4)  
include("GeneratorFilters/JetFilter_JZX.py")
JZSlice(4,filtSeq)  
