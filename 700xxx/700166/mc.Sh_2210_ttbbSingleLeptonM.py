include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO_nf_4.py")

evgenConfig.description = "Sherpa 2.2.10 ttbb production at NLO with t(had)tbar(l-) decays."
evgenConfig.keywords = ["SM", "top" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.nEventsPerJob = 200

genSeq.Sherpa_i.RunCard="""
(run){
  # ME and clustering options
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  QSF:=1;
  SCALES VAR{H_TM2/4}{0.25*sqrt(MPerp(p[2])*MPerp(p[3])*MPerp(p[4])*MPerp(p[5]))}{sqr(QSF)*H_TM2/4}
  CORE_SCALE VAR{MU_F2}{MU_R2}{sqr(QSF)*H_TM2/4}
  EXCLUSIVE_CLUSTER_MODE 1;
  CSS_RESPECT_Q2=1
  CSS_KMODE=34
  CSS_KIN_SCHEME=0

  # massive b parameters
  MASSIVE[5]=1
  MASS[5]=4.75
  CSS_SCALE_SCHEME 20
  CSS_EVOLUTION_SCHEME 30

  # decays
  HARD_DECAYS=1
  STABLE[24] 0; STABLE[6] 0; WIDTH[6] 0;
  HDH_STATUS[24,12,-11]=0
  HDH_STATUS[24,14,-13]=0
  HDH_STATUS[24,16,-15]=0
  HDH_STATUS[-24,-2,1]=0
  HDH_STATUS[-24,-4,3]=0
  SPECIAL_TAU_SPIN_CORRELATIONS=1
  SOFT_SPIN_CORRELATIONS=1

  NLO_CSS_PSMODE=1
}(run)

(processes){
  Process 93 93 -> 6 -6 5 -5
  Order (*,0)
  NLO_QCD_Mode MC@NLO
  Loop_Generator LOOPGEN
  ME_Generator Amegic
  RS_ME_Generator Comix
  End process
}(processes)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0" ]

genSeq.Sherpa_i.NCores = 24
genSeq.Sherpa_i.CleanupGeneratedFiles = 1

# add also 0.0625*muF2 variation
genSeq.Sherpa_i.Parameters += [ "SCALE_VARIATIONS=0.25,0.25 0.25,1. 1.,0.25 1.,1. 1.,4. 4.,1. 4.,4. 0.25,0.0625 1.,0.0625" ]
