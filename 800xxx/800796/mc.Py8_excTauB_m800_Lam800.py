####################################                                                                                                                                                                           
#
# JO file for production of excited tau-leptons signal samples with Pythia8                                                                                                                                    
# pp coll. -> SM tau + excited tau -> SM tau + SM tau + b-jet + b-jet                                                                                                                                         
#
#===================================
evgenConfig.description = "Single excited tau-lepton produced via contact interaction with b-bbar incoming, the A14 tune and NNPDF23LO PDF"
evgenConfig.process = "p + p -> gluon splitting -> b + bbar -> SMtau + tau*(-> SMtau + bjet + bjet)"
evgenConfig.keywords = ["exotic","contactInteraction","contactinteraction","BSM","compositeness","excited","exclusive","tau","2tau","multilepton","dijet","2jet","multijet"]
evgenConfig.contact = ["Krystsina Petukhova <kristina.mihule@cern.ch>"]
evgenConfig.generators += ["Pythia8"]

M_ExLep=800
M_Lam=800
evgenConfig.nEventsPerJob=1000

# Excited tau-lepton MC PDG ID                                                                                                                                                                                
leptID = 4000015
# SM tau-lepton ID                                                                                                                                                                                            
SMlepID = 15

# Contact interaction: coupling constants                                                                                                                                                                     
f = 1.0 # Strength f of the SU(2), SU(3) coupling.                                                                                                                                                            
fPrime = 1.0 # Strength f' of the U(1) coupling.                                                                                                                                                              
etaLL = 1.0 # Strength for CI left-left current                                                                                                                                                               

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["ExcitedFermion:qqbar2tauStartau = on", #Scatterings q qbar to tau^*+- tau^-+                                                                                                     
                            str(leptID)+":m0 = "+str(M_ExLep), # Setting mass of excited tau-lepton                                                                                                           
                            str(leptID)+':mayDecay = on',
                            # switchin on only excited tau decays to b-quarks via CI                                                                                                                          
                            str(leptID)+":onMode = off",
                            str(leptID)+":onIfAll = -5 5 "+str(SMlepID),
                            "ExcitedFermion:Lambda = "+str(M_Lam), # Compositeness scale                                                                                                                      
                            "ExcitedFermion:coupF = "+str(f), # Strength f of the SU(2) coupling.                                                                                                             
                            "ExcitedFermion:coupFprime = "+str(fPrime), # Strength f' of the U(1) coupling.                                                                                                   
                            "ExcitedFermion:coupFcol = "+str(f), #Strength f_c of the SU(3) coupling.                                                                                                         
                            "ContactInteractions:etaLL = "+str(etaLL), # Factors for left-left currents                                                                                                        
                        ]

from GeneratorFilters.GeneratorFiltersConf import SameParticleHardScatteringFilter
filtSeq += SameParticleHardScatteringFilter("SameParticleHardScatteringFilter")
filtSeq.SameParticleHardScatteringFilter.PDGParent = [-5,5]  # Select b-bbar scattering                                                                                                                       
filtSeq.SameParticleHardScatteringFilter.PDGChild = [4000015]   # Select bbar fusion to excited tau 

#===================================                                                                                                                                                                          
#                                                                                                                                                                                                             
# End of job options file                                                                                                                                                                                     
# 
