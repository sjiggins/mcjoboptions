# include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_A14_NNPDF23LO_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off',
                             '25:addChannel = 1 0.002 100 223  22'
                             ]


evgenConfig.process        = 'ttH H->OmegaGamma'
evgenConfig.description    = 'aMcAtNlo Pythia8 ttH allhad, H to Omega'
evgenConfig.keywords       = [ 'SM', 'Higgs', 'SMHiggs', 'mH125', 'ttHiggs','photon' ]
evgenConfig.contact = ["Govindraj Singh Virdee <g.s.virdee@cern.ch>"]
evgenConfig.generators  = [ "aMcAtNlo", "Pythia8"]
evgenConfig.inputFilesPerJob = 2 #Specify the number of LHEs files needed
evgenConfig.nEventsPerJob = 10000

