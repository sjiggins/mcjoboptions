include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

from MadGraphControl.MadGraphUtilsHelpers import *
JOName = get_physics_short()

#get the file name and split it on _ to extract relavent information                                                                                                                                       

jobConfigParts = JOName.split("_")

if "MadSpin" in JOName:
    mstop=float(jobConfigParts[5])
    mneutralino=float(jobConfigParts[6].split('.')[0])
else:
    mstop=float(jobConfigParts[4])
    mneutralino=float(jobConfigParts[5].split('.')[0])

masses['1000006'] = mstop
masses['1000022'] = mneutralino

process = '''
generate p p > t1 t1~ $ go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @1
add process p p > t1 t1~ j $ go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @2
add process p p > t1 t1~ j j $ go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @2
'''

decays['1000006'] = """DECAY   1000006     1.34259598E-01   # stop1 decays
     0.50000000E+00    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     0.50000000E+00    2     1000022         4   # BR(~t_1 -> ~chi_10 c )
"""


njets = 2

if 'MET' in JOName.split('_')[-1]:
    include ( 'GeneratorFilters/MissingEtFilter.py' )

    metFilter = JOName.split('_')[-1]
    metFilter = int(metFilter.split("MET")[1].split(".")[0])
   
    print "Using MET Filter: " + str(metFilter)
    filtSeq.MissingEtFilter.METCut = metFilter*GeV
    evt_multiplier = metFilter / 10

else:
    print "No MET Filter applied"
               
       
evgenLog.info('Registered generation of stop pair production, stop to c+LSP and t+LSP; grid point '+str(runArgs.jobConfig[0])+' decoded into mass point mstop=' + str(masses['1000006']) + ', mlsp='+str(masses['1000022']))
 
evgenConfig.contact  = [ "alvaro.lopez.solis@cern.ch","armin.fehr@lhep.unibe.ch","john.kenneth.anders@cern.ch" ]
evgenConfig.keywords += ['simplifiedModel','charm']
evgenConfig.description = 'stop direct pair production, st->t+LSP and st-> c+LSP in simplified model'

if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = pp>{t1,1000006}{t1~,-1000006}"]

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )
