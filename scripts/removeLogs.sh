#!/bin/bash

# Check if log.generate.short files are present
logs=($(find . -name 'log.generate.short'))

if (( ${#logs[@]} > 0 )) ; then
    for logFile in "${logs[@]}" ; do git rm -f $logFile ; done
    git commit -m "Pipeline for $CI_COMMIT_SHORT_SHA succeeded. Removing log.generate files [skip all]"
    git remote set-url origin "https://gitlab-ci-token:${CI_UPLOAD_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git"
    git push origin ${CI_COMMIT_REF_NAME}
    exit $?
else
    echo "No log.generate.short files present. Nothing to be done"
fi

exit 0
