evgenConfig.generators   += ["Powheg","Pythia8","EvtGen"]
evgenConfig.description   = 'Powheg bb4l test production with A14 tune, NNPDF30 PDF, only e-mu+'
evgenConfig.keywords     += [ 'SM', 'top', 'WW', 'lepton']
evgenConfig.contact       = [ 'Simone Amoroso <simone.amoroso@cern.ch>', 'jan.kretzschmar@cern.ch' ]
evgenConfig.nEventsPerJob = 500

# -------------------- # Load ATLAS defaults for the Powheg bblvlv process # ----------------------------
include("PowhegControl/PowhegControl_bblvlv_Common.py")
PowhegConfig.width_t         = 1.3292779909405845
PowhegConfig.twidth_phsp     = 1.3292779909405845
PowhegConfig.hdamp           = 258.75 
PowhegConfig.for_reweighting = 1

### PDFs as in main hvq sample + NNPDF3.0 replica + a few newer ones
PowhegConfig.PDF             = [260000, 25200, 13165, 90900, 265000, 266000, 303400] # NNPDF30_nlo_as_0118, MMHT2014nlo68clas118, CT14nlo_as_0118, PDF4LHC15_nlo_30, NNPDF30_nlo_as_0117, NNPDF30_nlo_as_0119, NNPDF31_nlo_as_0118
PowhegConfig.PDF.extend(range(260001, 260101))         # Include the NNPDF3.0 error set
PowhegConfig.PDF.extend([303600, 14400, 14000, 14200, 27100, 27400]) # NNPDF31_nnlo_as_0118, CT18NLO, CT18NNLO, CT18ANNLO, MSHT20nlo_as118, MSHT20nnlo_as118

### Optimised fold and integration parameters
PowhegConfig.foldcsi      = 2
PowhegConfig.foldphi      = 2
PowhegConfig.foldy        = 2

PowhegConfig.ncall1       = 120000*4
PowhegConfig.ncall2       = 180000*4
PowhegConfig.nubound      = 100000*4
PowhegConfig.itmx1        = 1*4
PowhegConfig.itmx2        = 6*2
#PowhegConfig.xupbound     = 4.

PowhegConfig.nEvents = runArgs.maxEvents*1.05 if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob*1.05

# Experimental: try reweight top-quark mass and width
PowhegConfig.define_event_weight_group( group_name='topmass', parameters_to_vary=['mass_t'] )
PowhegConfig.add_weight_to_group( group_name='topmass', weight_name='mt169p0', parameter_values=[ 169.0 ] )
PowhegConfig.add_weight_to_group( group_name='topmass', weight_name='mt170p0', parameter_values=[ 170.0 ] )
PowhegConfig.add_weight_to_group( group_name='topmass', weight_name='mt171p0', parameter_values=[ 171.0 ] )
PowhegConfig.add_weight_to_group( group_name='topmass', weight_name='mt171p5', parameter_values=[ 171.5 ] )
PowhegConfig.add_weight_to_group( group_name='topmass', weight_name='mt172p0', parameter_values=[ 172.0 ] )
PowhegConfig.add_weight_to_group( group_name='topmass', weight_name='mt173p0', parameter_values=[ 173.0 ] )
PowhegConfig.add_weight_to_group( group_name='topmass', weight_name='mt173p5', parameter_values=[ 173.5 ] )
PowhegConfig.add_weight_to_group( group_name='topmass', weight_name='mt174p0', parameter_values=[ 174.0 ] )
PowhegConfig.add_weight_to_group( group_name='topmass', weight_name='mt175p0', parameter_values=[ 175.0 ] )
PowhegConfig.add_weight_to_group( group_name='topmass', weight_name='mt176p0', parameter_values=[ 176.0 ] )

PowhegConfig.define_event_weight_group( group_name='topwidth', parameters_to_vary=['width_t'] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt0p5', parameter_values=[ 0.5 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt0p8', parameter_values=[ 0.8 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt1p0', parameter_values=[ 1.0 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt1p2', parameter_values=[ 1.2 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt1p4', parameter_values=[ 1.4 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt1p6', parameter_values=[ 1.6 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt1p8', parameter_values=[ 1.8 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt2p0', parameter_values=[ 2.0 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt2p2', parameter_values=[ 2.2 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt2p4', parameter_values=[ 2.4 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt2p6', parameter_values=[ 2.6 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt2p8', parameter_values=[ 2.8 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt3p0', parameter_values=[ 3.0 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt3p5', parameter_values=[ 3.5 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt4p0', parameter_values=[ 4.0 ] )

# --------------------- # Generate events # --------------------------------------------------------------
PowhegConfig.generate()


################################################################
#                      adding UserHook                         #
################################################################         
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg.py")

genSeq.Pythia8.Commands += [ 'UncertaintyBands:overSampleFSR=10' ]
genSeq.Pythia8.Commands += [ 'UncertaintyBands:FSRpTmin2Fac=8' ]
genSeq.Pythia8.Commands += [ 'UncertaintyBands:overSampleISR=10' ]

if "UserHooks" in genSeq.Pythia8.__slots__.keys():
    print 'UserHook present'
    genSeq.Pythia8.UserHooks += ['PowhegBB4Ltms']

genSeq.Pythia8.Commands += ["POWHEG:veto=1"]
genSeq.Pythia8.Commands += ["POWHEG:vetoCount = 3"]
genSeq.Pythia8.Commands += ["POWHEG:pThard = 0"]
genSeq.Pythia8.Commands += ["POWHEG:pTemt = 0"]
genSeq.Pythia8.Commands += ["POWHEG:emitted = 0"]
genSeq.Pythia8.Commands += ["POWHEG:pTdef = 1"]
genSeq.Pythia8.Commands += ["POWHEG:nFinal = -1"]
genSeq.Pythia8.Commands += ["POWHEG:MPIveto = 1"]
genSeq.Pythia8.Commands += ["POWHEG:QEDveto = 1"]

genSeq.Pythia8.Commands += ["POWHEG:bb4l:FSREmission:veto = 1"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:onlyDistance1 = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:vetoQED = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:vetoAtPL = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:dryRunFSR = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:FSREmission:vetoDipoleFrame = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:pTpythiaVeto = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:ScaleResonance:veto = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:PartonLevel:veto = 0"]
#genSeq.Pythia8.Commands += ["POWHEG:bb4l:PartonLevel:excludeFSRConflicting = false"]                                                                                                                              
genSeq.Pythia8.Commands += ["POWHEG:bb4l:pTminVeto = 0.8"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:DEBUG = 0"]

