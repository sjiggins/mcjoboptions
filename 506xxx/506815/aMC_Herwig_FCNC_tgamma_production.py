from MadGraphControl.MadGraphUtils import *
import os,subprocess,fileinput
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short

phys_short = get_physics_short()
# General settings
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

is_u_quark     = True if "tugamma" in phys_short else False
is_left_handed = True if "LH" in phys_short else False

name = 'FCNC_tugamLH_NLO'
if is_u_quark and not is_left_handed: name = 'FCNC_tugamRH_NLO'
if not is_u_quark:
    name = 'FCNC_tcgamRH_NLO' if not is_left_handed else 'FCNC_tcgamLH_NLO'

runName='mc.aMC_HW7_'+str(name)

restrict = "onlytugam" if is_u_quark else "onlytcgam"

process = """
import model TopFCNC-%s
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
generate p p > t a $$ t~ [QCD]
add process p p > t~ a $$ t [QCD]

output -f"""%(restrict)

process_dir = new_process(process)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")



#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version'  : '3.0',
           'parton_shower' :'HERWIGPP',
           'maxjetflavor'  :5,
           'dynamical_scale_choice': '10', #sum of the transverse mass divided by 2
         # 'store_rwgt_info':True,
           'nevents':int(nevents),
           }

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)
#
WW = 2.085000e+00

decays = {'24':"""DECAY  24  """+str(WW)+""" #WW
        #  BR             NDA  ID1    ID2   ...
        3.377000e-01   2    -1  2
        3.377000e-01   2    -3  4
        1.082000e-01   2   -11 12
        1.082000e-01   2   -13 14
        1.082000e-01   2   -15 16
        #"""}

coup = {}
if is_u_quark:
    coup = {
    'RCtW': 0.,
    'RCuW': 0.,
    'RCtB': 0.,
    'RCuB': 0.,

    }
    if is_left_handed: coup["RCtB"] = 12.0
    else: coup["RCuB"] = 12.0

else:
    coup = {
    'RCtcW': 0.,
    'RCctW': 0.,
    'RCtcB': 0.,
    'RCctB': 0.,

    }
    if is_left_handed: coup["RCtcB"] = 12.0
    else: coup["RCctB"] = 12.0


modify_param_card(process_dir=process_dir,params={'DECAY':decays, 'dim6': coup})

madspin_card=process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card,'w')

mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
set Nevents_for_max_weight 250 # number of events for the estimate of the max. weight
# set BW_cut 15                # cut on how far the particle can be off-shell
 set max_weight_ps_point 1000  # number of PS to estimate the maximum for each event
#
set seed %i
# specify the decay for the final state particles
decay t > w+ b, w+ > l+ vl
decay t~ > w- b~, w- > l- vl~
# running the actual code
launch"""%runArgs.randomSeed)
mscard.close()

print_cards()

# Cook the setscales file for the user defined dynamical scale
fileN = process_dir+'/SubProcesses/setscales.f'
mark  = '      elseif(dynamical_scale_choice.eq.10) then'
rmLines = ['ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc',
           'cc      USER-DEFINED SCALE: ENTER YOUR CODE HERE                                 cc',
           'cc      to use this code you must set                                            cc',
           'cc                 dynamical_scale_choice = 10                                    cc',
           'cc      in the run_card (run_card.dat)                                           cc',
           'write(*,*) "User-defined scale not set"',
           'stop 1',
           'temp_scale_id=\'User-defined dynamical scale\' ! use a meaningful string',
           'tmp = 0',
           'cc      USER-DEFINED SCALE: END OF USER CODE                                     cc'
           ]
for line in fileinput.input(fileN, inplace=1):
    toKeep = True
    for rmLine in rmLines:
        if line.find(rmLine) >= 0:
           toKeep = False
           break
    if toKeep:
        print line,
    if line.startswith(mark):
        print """
c         mass of the top quark (that is the most massive particle produced)
c         Q^2=m_top
          tmp=0d0
          do i=3,nexternal
             tmp=max(tmp, pp(0,i)*pp(0,i)-pp(3,i)*pp(3,i)-pp(2,i)*pp(2,i)-pp(1,i)*pp(1,i))
          enddo
          tmp=dsqrt(tmp)
          temp_scale_id='m_top'
     """
#### End of setscales cooking

generate(runArgs=runArgs,process_dir=process_dir)#,grid_pack=gridpack_mode,gridpack_dir=gridpack_dir,gridpack_compile=True)
outputDS = arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

#### Shower
evgenConfig.description = 'aMC@NLO_'+str(name)
evgenConfig.generators += ["aMcAtNlo", "Herwig7"]
evgenConfig.keywords+= ['FCNC', 'top',  'photon','singleTop','lepton']
evgenConfig.contact = ['Bjoern Wendland <bjorn.wendland@cern.ch>']
evgenConfig.tune = "H7.1-Default"
runArgs.inputGeneratorFile=outputDS+".events"

check_reset_proc_number(opts)
include("Herwig7_i/Herwig72_LHEF.py")

Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

include("Herwig7_i/Herwig71_EvtGen.py")

Herwig7Config.run()
