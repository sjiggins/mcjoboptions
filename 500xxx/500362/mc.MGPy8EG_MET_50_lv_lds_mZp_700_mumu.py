model="LightVector"
fs = "mumu"
mDM1 = 5.
mDM2 = 730.
mZp = 700.
mHD = 125.
filteff = 8.445233e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Even S. Haaland <even.simonsen.haaland@cern.ch>"]

include("MGPy8EG_mono_zp_lep.py")
