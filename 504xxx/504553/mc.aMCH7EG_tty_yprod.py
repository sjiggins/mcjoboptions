import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':260000, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets
    'pdf_variations':[260000,90900], # list of pdfs ids for which all variations (error sets) will be included as weights
    'alternative_pdfs':None, # list of pdfs ids for which only the central set will be included as weights
    'scale_variations':[0.5,1,2], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated
}

from MadGraphControl.MadGraphUtils import *

# General settings
nevents=int(2*1.1*runArgs.maxEvents)

process = """
import model loop_sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
generate p p > t t~ a QED=1 QCD=2 [QCD]
output -f"""

dec = """
define w+child = e+ mu+ ta+ ve vm vt u c d~ s~
define w-child = e- mu- ta- ve~ vm~ vt~ u~ c~ d s
decay t > w+ b, w+ > w+child w+child
decay t~ > w- b~, w- > w-child w-child
"""

settings = {'lhe_version'	:'3.0',
			'maxjetflavor'	:5,
			'parton_shower'	:'HERWIGPP',
			'ptl'			:20.,
			'ptgmin'		:15.,
			'R0gamma'		:0.1,
			'xn'			:2,
			'epsgamma'		:0.1,
			'ptj'			:1.,
			'etal'			:5.0,
			'etagamma'		:5.0,
			'dynamical_scale_choice':'3',
			'nevents'		:nevents
}

process_dir = new_process(process)
# Run card
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

# Decay with MadSpin
madspin_card_loc=process_dir+'/Cards/madspin_card.dat'
mscard = open(madspin_card_loc,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
set max_weight_ps_point 500  # number of PS to estimate the maximum for each event
set BW_cut 50
set seed %i
%s
launch
"""%(runArgs.randomSeed, dec))
mscard.close()

# Print cards
print_cards()
# set up
generate(runArgs=runArgs, process_dir=process_dir)
# run
outputDS=arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)

# Go to serial mode for Pythia8
if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = 0
    print opts

#--------------------------------------------------------------
# EVGEN Configuration
#--------------------------------------------------------------
keyword=['SM','top', 'ttgamma', 'photon']
evgenConfig.keywords += keyword
evgenConfig.generators += ["aMcAtNlo", "Herwig7", "EvtGen"]
evgenConfig.tune        = "H7.1-Default"
evgenConfig.description = 'aMcAtNlo+H7_ttgamma_nonallhad_GamFromProd'
evgenConfig.contact = ["amartya.rej@cern.ch","arpan.ghosal@cern.ch"]

runArgs.inputGeneratorFile=outputDS+".events"
# Striping PDF and scale factor weights
include("Herwig7_i/Herwig7_701_StripWeights.py")

#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig72_LHEF.py")
# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")
# run Herwig7
Herwig7Config.run()
#-------------------------------------------------------------------------------------
#Event filter
#-------------------------------------------------------------------------------------
print ("LEPTON FILTER APPLICATION")
include("GeneratorFilters/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1

