from MadGraphControl.MadGraphUtils import *

#
## 5 inputs parameters, mass of tetrab (m4b) and couplings
##
## Example 1: 18.4GeV tetrab (4b bound state) scalar --> Upsilon+2mu --> 4mu with 1 jet
##
## m4b=18.4
## cVV4=0.001
## cGG6=0.001
## tcVV6=0.0
## tcGG6=0.0
##
## Example 2: 18.4GeV tetrab (4b bound state) pseduoscalar --> Upsilon+2mu --> 4mu with 1 jet
##
## m4b=18.4
## cVV4=0.0
## cGG6=0.0
## tcVV6=0.001
## tcGG6=0.001
#
maxjetflavor=4

## safe factor applied to nevents, to account for the filter efficiency
safefactor=1.45

newPDG_file = open('tetrab_pdg_extras.dat','w') 
#  The most important number is the first: the PDGID of the particle 
newPDG_file.write('90000025\n')
newPDG_file.close()
testSeq.TestHepMC.UnknownPDGIDFile = 'tetrab_pdg_extras.dat'

# create the process string to be copied to proc_card_mg5.dat
process="""
import model HEL_4b_UFO
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
set group_subprocesses Auto
set ignore_six_quark_processes False
set gauge unitary
set complex_mass_scheme False
import model HEL_4b_UFO
generate p p > tetrab j, (tetrab > upsilon mu+ mu-, upsilon > mu+ mu-)
output -f
"""
print 'process string: ',process

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
  beamEnergy = runArgs.ecmEnergy / 2.
else: 
  raise RuntimeError("No center of mass energy found.")

#---------------------------------------------------------------------------
# MG5 run Card
#---------------------------------------------------------------------------
#
# ptj default value 20GeV, using ktdurham just in case its lower than 20GeV 
#
nevents=int(runArgs.maxEvents*safefactor)
extras = {
    'pdlabel'      : "'lhapdf'",
    'lhaid'        : '263000',
    'lhe_version'  : '3.0',
    'maxjetflavor' : maxjetflavor,
    'asrwgtflavor' : maxjetflavor,
    'nevents'      : nevents,
    'ptj'          : 15.0,
    'ptl'          : 0.0,
    'etal'         : -1.0,
    'drll'         : 0.0,
    'drjl'         : 0.0,
    'drbl'         : 0.0,
    'dral'         : 0.0,
    'cut_decays'   : 'F',
    'use_syst'     : 'T',
    'sys_alpsfact' : '0.5 1 2',
    'sys_pdf'      : 'CT14nlo && MMHT2014nlo68clas118' #'NNPDF23_lo_as_0130_qed'
}

process_dir = new_process(process)

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------
# MG5 param Card
#---------------------------------------------------------------------------
## params is a dictionary of dictionaries (each dictionary is a separate block)
## parameters for newcoup
ncoups={}
ncoups['cVV4']=cVV4
ncoups['cGG6']=cGG6
ncoups['tcVV6']=tcVV6
ncoups['tcGG6']=tcGG6
## mass parameters
masses={}
masses['90000025']=m4b

params={}
params['mass']=masses
params['newcoup']=ncoups

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# Build a new param_card.dat from an existing one
#---------------------------------------------------------------------------------------------------
modify_param_card(process_dir=process_dir,params=params)

print_cards()

runName='run_01'

# and the generation
generate(process_dir=process_dir,grid_pack=False,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

import os
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts                                                                                                                                                                       
#### Shower                                                                                                                                                             
evgenConfig.description = "4b Tetraquark Meson, https://arxiv.org/pdf/1709.09605.pdf"
evgenConfig.keywords = ["heavyFlavour","Upsilon","4muon"]
evgenConfig.process = "p p > tetrab j, (tetrab > upsilon mu+ mu-, upsilon > mu+ mu-)"
evgenConfig.contact = ["Tiesheng Dai <tiesheng.dai@cern.ch>"]
evgenConfig.inputfilecheck = runName
evgenConfig.inputfilecheck = ""

##############################################################
# Python snippet to generate EvtGen user decay file on the fly
# Upsilon -> mumu
##############################################################
f = open("MY_UPSILON_USER_FORCED.DEC","w")
f.write("Decay Upsilon\n")
f.write("1.0000    mu+  mu-             PHOTOS  VLL; #[Reconstructed PDG2011]\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()
##############################################################
                                                                                                             
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

genSeq.Pythia8.Commands += [
                            '553:onMode = off'
                           ]
genSeq.EvtInclusiveDecay.userDecayFile = "MY_UPSILON_USER_FORCED.DEC"
#
### 3 Muon filter
#
from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
filtSeq += MultiMuonFilter("MultiMuonFilter")
filtSeq.MultiMuonFilter.NMuons = 3
filtSeq.MultiMuonFilter.Ptcut = 2000.0
filtSeq.MultiMuonFilter.Etacut = 3.0

