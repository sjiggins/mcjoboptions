from MadGraphControl.MadGraphUtils import *
mode=0

shortname = jofile.rstrip('.py').split('_')

Y1decay=shortname[3]
Hdecay=shortname[4]
resonanceMass=int(shortname[5])
resonanceWidthPercent=shortname[6]

print 'Y1decay: %s' % Y1decay
print 'Hdecay: %s' % Hdecay
print 'resonanceMass: %d' % resonanceMass
print 'resonanceWidthPercent: %s' % resonanceWidthPercent

resonanceWidth = 0

if resonanceWidthPercent == "NW":
    resonanceWidth = 0.0042  #GeV
else:
    resonanceWidthPercent = float(resonanceWidthPercent[1:-1]) * 0.01 #Converting string to percent of mass
    resonanceWidth = resonanceWidthPercent * resonanceMass

if resonanceWidth == 0:
    raise RuntimeError("Resonance width not correctly interpreted from JO name")


#---------------------------------------------------------------------------------------------------
# Setting X0 mass and width for param_card.dat
#---------------------------------------------------------------------------------------------------
Y1Mass  = {'5000001': '%e # MY1'%resonanceMass}               #Mass
Y1Decay = {'5000001':'DECAY 5000001 %e # WY1'%resonanceWidth} #Width

#---------------------------------------------------------------------------------------------------
# Generating pp(qq) -> Y1 -> Hy  in DM spin-1 Z' effective coupling model
#---------------------------------------------------------------------------------------------------
process ="""
    import model ZpHA
    define q = u c d s b u~ c~ d~ s~ b~
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > y1 > h a
    output -f"""

process_dir = new_process(process)
print('Using process directory '+str(process_dir))

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

evgenConfig.description = "Dark Matter Zprime model LO Spin-1 qq->Y1->Higgs+gamma 500 GeV narrow width resonance in gamma-gamma decay model"
evgenConfig.keywords = ["exotic", "Higgs", "photon", "LO", "bbbar", "spin1"]

#---------------------------------------------------------------------------------------------------
# Setting the number of generated events to 'safefactor' times maxEvents,
# to avoid crashing due to not having enough events
# Also putting protection to avoid from crashing when maxEvents=-1
#---------------------------------------------------------------------------------------------------
safefactor = 1.1
nevents    = 5000*safefactor
if runArgs.maxEvents > 0:
    nevents=runArgs.maxEvents*safefactor

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# Build a new param_card.dat from an existing one
#---------------------------------------------------------------------------------------------------

param_card_extras = {}
param_card_extras['MASS'] = Y1Mass # changing mass in the param_card 
param_card_extras['DECAY'] = Y1Decay # changing mass in the param_card 
modify_param_card(param_card_input=None,process_dir=process_dir, params=param_card_extras)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the run_card
# Build a new run_card.dat from an existing one
# Using the values given in "extras" above for the selected parameters when setting up the run_card
# If not set in "extras", default values are used
#---------------------------------------------------------------------------------------------------
extras = {'lhe_version':'3.0',
          'nevents':int(nevents)}

extras["scale"] = resonanceMass
extras["dsqrt_q2fact1"] = resonanceMass
extras["dsqrt_q2fact2"] = resonanceMass
extras["pdlabel"]='lhapdf'
extras["lhaid"]=247000

modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=extras)

print_cards()

generate(runArgs=runArgs,process_dir=process_dir)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

#### Shower

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = 'Dark Matter Zprime model LO Spin-1 qq->Y1->Higgs+gamma narrow width resonance in yy decay model'
evgenConfig.keywords = ["exotic", "Higgs", "photon", "LO", "spin1"]
evgenConfig.contact = ['Shu Li <Shu.Li@cern.ch>']

if ("aa" in Hdecay):
    genSeq.Pythia8.Commands += ['25:onMode = off',
                                '25:onIfMatch = 22 22']


#runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'
