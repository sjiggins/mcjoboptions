mR          = 30
mDM         = 10000
gVSM        = 0.00
gASM        = 0.20
gVDM        = 0.00
gADM        = 1.00
filteff     = 0.002000
phminpt     = 130
quark_decays= ['b']

evgenConfig.nEventsPerJob = 500

include("MadGraphControl_MGPy8EG_DMS1_dijetgamma.py")

evgenConfig.description = "Zprime with ISR - mR30 - model DMsimp_s_spin1"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Chris Delitzsch <chris.malena.delitzsch>, Karol Krizka <kkrizka@cern.ch>"]

