include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

evgenConfig.description = "Dijet truth jet slice JZ1, with the A14 NNPDF23 LO tune, mu-filtered, using Hard QCD process"
evgenConfig.keywords = ["QCD", "jets", "SM"]
evgenConfig.contact = ["valentina.vecchio@cern.ch","bingxuan.liu@cern.ch"]

genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:Bias2Selection=on",
                            "PhaseSpace:pTHatMin = 5."]

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.6)    
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.6)  
include("GeneratorFilters/JetFilter_JZX.py")
JZSlice(1,filtSeq) 
include("GeneratorFilters/LowPtMuonFilter.py")
evgenConfig.nEventsPerJob = 1000

