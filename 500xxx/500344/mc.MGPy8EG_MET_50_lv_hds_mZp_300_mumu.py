model="LightVector"
fs = "mumu"
mDM1 = 150.
mDM2 = 600.
mZp = 300.
mHD = 125.
filteff = 9.648784e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Even S. Haaland <even.simonsen.haaland@cern.ch>"]

include("MGPy8EG_mono_zp_lep.py")
