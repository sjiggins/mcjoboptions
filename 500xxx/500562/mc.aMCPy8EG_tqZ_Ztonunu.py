from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphParamHelpers import set_top_params
import fileinput
import shutil
import subprocess
import os
import sys
# General settings

evgenConfig.nEventsPerJob=5000
nevents = int(1.1*runArgs.maxEvents) if runArgs.maxEvents > 0 else int(1.1*evgenConfig.nEventsPerJob)

parton_shower="PYTHIA8"

# MG merging settings
maxjetflavor=4
ickkw=0
dyn_scale = '10'

gen_process = """
import model loop_sm
define tt = t t~
define bb = b b~
generate p p > tt bb j Z $$ W+ W- [QCD]
output -f
"""

gridpack_mode=True
process_dir='madevent/'

if not is_gen_from_gridpack():
  process_dir = new_process(gen_process)
else:
  process_dir = MADGRAPH_GRIDPACK_LOCATION

set_top_params(process_dir,mTop=172.5,FourFS=True)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

# Decay with MadSpin

if is_gen_from_gridpack():
    fMadSpinCard=open(process_dir+'/Cards/madspin_card.dat','w')
    fMadSpinCard.write('''set Nevents_for_max_weigth 250 # number of events for the estimate of the max. weight (default: 75)
set max_weight_ps_point 1000  # number of PS to estimate the maximum for each event (default: 400)
decay t > w+ b, w+ > all all
decay t~ > w- b~, w- > all all
decay w+ > all all
decay w- > all all
decay z > vl vl~
launch''')
    fMadSpinCard.close()


pdflabel="lhapdf"
lhaid=260400
#Fetch default LO run_card.dat and set parameters
run_settings = { 'lhe_version'  : '3.0',
           'pdlabel'      : "'"+pdflabel+"'",
           'lhaid'        : lhaid,
           'maxjetflavor' : maxjetflavor,
           'nevents'      : nevents,
           'ickkw'        : 0,
           'ptj'          : 0,
           'drll'         : 0.0,
           'ptl'          : 0,
           'etal'         : 10.0,
           'etaj'         : 10.0,
           'bwcutoff'      : 50,          
           'dynamical_scale_choice' : dyn_scale,
           'reweight_PDF': 'True',
           'PDF_set_min': 260401,
           'PDF_set_max':260500,
           'store_rwgt_info':True,
           'parton_shower': parton_shower,

 }


modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=run_settings)


# Cook the setscales file for the user defined dynamical scale
fileN = process_dir+'/SubProcesses/setscales.f'


mark = '      elseif(dynamical_scale_choice.eq.10) then'
rmLines = ['ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc',
           'cc      USER-DEFINED SCALE: ENTER YOUR CODE HERE                                 cc',
           'cc      to use this code you must set                                            cc',
           'cc                 dynamical_scale_choice = 10                                   cc',
           'cc      in the run_card (run_card.dat)                                           cc',
           'ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc',
           'write(*,*) "User-defined scale not set"',
           'stop 1',
           'temp_scale_id=\'User-defined dynamical scale\' ! use a meaningful string',
           'tmp = 0d0']

dyn_scale_fact = 1.0

flag=0
for line in fileinput.input(fileN, inplace=1):
    toKeep = True
    for rmLine in rmLines:
        if line.find(rmLine) >= 0:
           toKeep = False
           break
    if toKeep:
        print line,
    if line.startswith(mark) and flag==0:
        flag +=1
        print """
c         Q^2= mb^2 + 0.5*(pt^2+ptbar^2)
c          rscale=0d0             !factorization scale**2 for pdf1
c                                  !factorization scale**2 for pdf2
c          xmtc=dot(P(0,6),P(0,6))
c          rscale = 4d0 * sqrt(pt(P(0,6))**2+xmtc)
          do i=3,nexternal
            xm2=dot(pp(0,i),pp(0,i))
            if ( xm2 < 30 .and. xm2 > 10 ) then
              tmp = 4d0 * dsqrt(pt(pp(0,i))**2+xm2)
c write(*,*) i, pt(P(0,i))**2, xmtc, sqrt(pt(P(0,i))**2+xmtc), rscale
c write(*,*) i, xmtc, pt(P(0,i)), rscale
            endif
          enddo
          tmp = %f*tmp""" % dyn_scale_fact  



generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs,required_accuracy=0.001)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)
evgenConfig.description = "MG5aMCatNLO/MadSpin/Pythia8 tZq NLO 4F with Z->nunu"
evgenConfig.keywords    = ["SM","top"]
evgenConfig.contact     = ["olga.bylund@cern.ch","maria.moreno.llacer@cern.ch"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")
