import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment
from MadGraphControl.MadGraphUtils import *

# WZZ -> 5l
evgenConfig.nEventsPerJob=5000
evgenConfig.contact = ["thomas.glyn.hitchings@cern.ch"]
runName = 'test'
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

process = """
import model SM_Ltotal_Ind5v2020v2_UFO
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define wpm = w+ w-
define l = e- mu- e+ mu+
define v = ve ve~ vm vm~
generate p p > wpm z z QCD=1 QED=3 NP=1
output -f"""
process_dir = new_process(process)


settings = {'nevents':int(nevents)}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

madspin_card=process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weight 75 # number of events for the estimate of the max. weight
# set BW_cut 15                # cut on how far the particle can be off-shell
# set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
# specify the decay for the final state particles
decay z > l l
decay z > l l
decay wpm > l v
# running the actual code
launch"""%runArgs.randomSeed)
mscard.close()

#update param card to include new couplings
param_card_name = 'param_card_mod.dat'
modify_param_card(param_card_input=param_card_name,process_dir=process_dir)

rcard = open('reweight_card.dat','w')
reweightCommand="""
launch --rwgt_name=test_rw_SM
  set anoinputs 1 0
  set anoinputs 2 0
  set anoinputs 3 0
  set anoinputs 4 0
  set anoinputs 5 0
  set anoinputs 6 0
  set anoinputs 7 0
  set anoinputs 8 0
  set anoinputs 9 0
  set anoinputs 10 0
  set anoinputs 11 0
  set anoinputs 12 0
  set anoinputs 13 0
  set anoinputs 14 0
  set anoinputs 15 0
  set anoinputs 16 0
  set anoinputs 17 0
  set anoinputs 18 0
  set anoinputs 19 0
  set anoinputs 20 0
  set anoinputs 21 0
  
launch --rwgt_name=M3_40
  set anoinputs 1 0
  set anoinputs 2 0
  set anoinputs 3 0
  set anoinputs 4 0
  set anoinputs 5 0
  set anoinputs 6 0
  set anoinputs 7 40e-12
  set anoinputs 8 0
  set anoinputs 9 0
  set anoinputs 10 0
  set anoinputs 11 0
  set anoinputs 12 0
  set anoinputs 13 0
  set anoinputs 14 0
  set anoinputs 15 0
  set anoinputs 16 0
  set anoinputs 17 0
  set anoinputs 18 0
  set anoinputs 19 0
  set anoinputs 20 0
  set anoinputs 21 0

launch --rwgt_name=M3_30
  set anoinputs 1 0
  set anoinputs 2 0
  set anoinputs 3 0
  set anoinputs 4 0
  set anoinputs 5 0
  set anoinputs 6 0
  set anoinputs 7 30e-12
  set anoinputs 8 0
  set anoinputs 9 0
  set anoinputs 10 0
  set anoinputs 11 0
  set anoinputs 12 0
  set anoinputs 13 0
  set anoinputs 14 0
  set anoinputs 15 0
  set anoinputs 16 0
  set anoinputs 17 0
  set anoinputs 18 0
  set anoinputs 19 0
  set anoinputs 20 0
  set anoinputs 21 0
  
launch --rwgt_name=M3_20
  set anoinputs 1 0
  set anoinputs 2 0
  set anoinputs 3 0
  set anoinputs 4 0
  set anoinputs 5 0
  set anoinputs 6 0
  set anoinputs 7 20e-12
  set anoinputs 8 0
  set anoinputs 9 0
  set anoinputs 10 0
  set anoinputs 11 0
  set anoinputs 12 0
  set anoinputs 13 0
  set anoinputs 14 0
  set anoinputs 15 0
  set anoinputs 16 0
  set anoinputs 17 0
  set anoinputs 18 0
  set anoinputs 19 0
  set anoinputs 20 0
  set anoinputs 21 0
  
launch --rwgt_name=M3_10
  set anoinputs 1 0
  set anoinputs 2 0
  set anoinputs 3 0
  set anoinputs 4 0
  set anoinputs 5 0
  set anoinputs 6 0
  set anoinputs 7 10e-12
  set anoinputs 8 0
  set anoinputs 9 0
  set anoinputs 10 0
  set anoinputs 11 0
  set anoinputs 12 0
  set anoinputs 13 0
  set anoinputs 14 0
  set anoinputs 15 0
  set anoinputs 16 0
  set anoinputs 17 0
  set anoinputs 18 0
  set anoinputs 19 0
  set anoinputs 20 0
  set anoinputs 21 0
  
launch --rwgt_name=M3_m10
  set anoinputs 1 0
  set anoinputs 2 0
  set anoinputs 3 0
  set anoinputs 4 0
  set anoinputs 5 0
  set anoinputs 6 0
  set anoinputs 7 -10e-12
  set anoinputs 8 0
  set anoinputs 9 0
  set anoinputs 10 0
  set anoinputs 11 0
  set anoinputs 12 0
  set anoinputs 13 0
  set anoinputs 14 0
  set anoinputs 15 0
  set anoinputs 16 0
  set anoinputs 17 0
  set anoinputs 18 0
  set anoinputs 19 0
  set anoinputs 20 0
  set anoinputs 21 0
  
launch --rwgt_name=M3_m20
  set anoinputs 1 0
  set anoinputs 2 0
  set anoinputs 3 0
  set anoinputs 4 0
  set anoinputs 5 0
  set anoinputs 6 0
  set anoinputs 7 -20e-12
  set anoinputs 8 0
  set anoinputs 9 0
  set anoinputs 10 0
  set anoinputs 11 0
  set anoinputs 12 0
  set anoinputs 13 0
  set anoinputs 14 0
  set anoinputs 15 0
  set anoinputs 16 0
  set anoinputs 17 0
  set anoinputs 18 0
  set anoinputs 19 0
  set anoinputs 20 0
  set anoinputs 21 0
  
launch --rwgt_name=M3_m30
  set anoinputs 1 0
  set anoinputs 2 0
  set anoinputs 3 0
  set anoinputs 4 0
  set anoinputs 5 0
  set anoinputs 6 0
  set anoinputs 7 -30e-12
  set anoinputs 8 0
  set anoinputs 9 0
  set anoinputs 10 0
  set anoinputs 11 0
  set anoinputs 12 0
  set anoinputs 13 0
  set anoinputs 14 0
  set anoinputs 15 0
  set anoinputs 16 0
  set anoinputs 17 0
  set anoinputs 18 0
  set anoinputs 19 0
  set anoinputs 20 0
  set anoinputs 21 0
  
launch --rwgt_name=M3_m40
  set anoinputs 1 0
  set anoinputs 2 0
  set anoinputs 3 0
  set anoinputs 4 0
  set anoinputs 5 0
  set anoinputs 6 0
  set anoinputs 7 -40e-12
  set anoinputs 8 0
  set anoinputs 9 0
  set anoinputs 10 0
  set anoinputs 11 0
  set anoinputs 12 0
  set anoinputs 13 0
  set anoinputs 14 0
  set anoinputs 15 0
  set anoinputs 16 0
  set anoinputs 17 0
  set anoinputs 18 0
  set anoinputs 19 0
  set anoinputs 20 0
  set anoinputs 21 0

launch --rwgt_name=M3_m44
  set anoinputs 1 0
  set anoinputs 2 0
  set anoinputs 3 0
  set anoinputs 4 0
  set anoinputs 5 0
  set anoinputs 6 0
  set anoinputs 7 -44e-12
  set anoinputs 8 0
  set anoinputs 9 0
  set anoinputs 10 0
  set anoinputs 11 0
  set anoinputs 12 0
  set anoinputs 13 0
  set anoinputs 14 0
  set anoinputs 15 0
  set anoinputs 16 0
  set anoinputs 17 0
  set anoinputs 18 0
  set anoinputs 19 0
  set anoinputs 20 0
  set anoinputs 21 0

"""
rcard.write(reweightCommand)
rcard.close()

subprocess.call('cp reweight_card.dat ' + process_dir+'/Cards/', shell=True)

generate(process_dir=process_dir,runArgs=runArgs)
outDS = arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

#### Shower
evgenConfig.generators = ["aMcAtNlo"]
evgenConfig.description = 'aMcAtNlo_WWZ'
evgenConfig.keywords+=['lepton']
#evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=outDS #'test_lhe_events.events'

# SHOWERING
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

#LO shower
include("Pythia8_i/Pythia8_MadGraph.py")

#### Finalize
#theApp.finalize()
#theApp.exit:()
