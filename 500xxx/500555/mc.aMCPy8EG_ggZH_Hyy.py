evgenConfig.keywords    = ['BSM', 'Higgs', 'mH125', 'ZH', 'resonance', 'jets']
evgenConfig.contact     = ['ana.cueto@cern.ch']
evgenConfig.generators  = ['aMcAtNlo', 'Pythia8', 'EvtGen']
evgenConfig.description = """ggZH 125 GeV Higgs production decaying to gammagamma."""
#evgenConfig.nEventsPerJob = 100



import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

# General settings                                                                                                                           
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

gridpack_mode=True
if not is_gen_from_gridpack():
    process = """                
    import model loop_sm-no_b_mass
    define p = g u c b d s u~ c~ d~ s~ b~                                                                                                           
    define j = g u c b d s u~ c~ d~ s~ b~                                                                                                           
    define l+ = e+ mu+ ta+                                                                                                                          
    define l- = e- mu- ta-                                                                                                                          
    define vl~ = ve~ vm~ vt~                                                                                                                        
    define vl = ve vm vt                                                                                                                            
    generate g g > h l+ l- [noborn=QCD]  
    add process g g >  h vl~ vl [noborn=QCD]
    output -f                                                                                                                                
    """

    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION
settings= {
          'ptj'           : 10,
          'maxjetflavor'  : 5,
#          'parton_shower' :'PYTHIA8', 
#          'mll_sf'        : 10.0,
#          'muR_ref_fixed' : 125.0,
#          'muF1_ref_fixed': 125.0,
#          'muF2_ref_fixed': 125.0,
#          'QES_ref_fixed' : 125.0,
          'nevents'      :int(nevents)
          }

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

masses={'25': '1.250000e+02'}
params={}
params['MASS']=masses
modify_param_card(process_dir=process_dir,params=params)


generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)


print "Now performing parton showering ..."
######################################################################
# End of event generation, start configuring parton shower here.
######################################################################
'''
if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
else: opts.nprocs = 0
    print opts
'''

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")   

genSeq.Pythia8.Commands  += [ '25:onMode = off', '25:onIfMatch = 22 22' ] #Decay
