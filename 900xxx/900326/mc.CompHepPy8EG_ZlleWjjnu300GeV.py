#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.generators += [ "Lhef", "Pythia8", "EvtGen" ]
evgenConfig.description = "E6 Vector Like Lepton Pair Production"
evgenConfig.keywords = ["exotic"]
evgenConfig.contact = ['Bora Orgen <bora.orgen@cern.ch>']
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_LHEF.py")

genSeq.Pythia8.Commands += [ "24:onMode = off","24:onIfAny = 1 2 3 4 5",  # decay of W
                             "23:onMode = off","23:onIfAny = 11 13" ]    # decay of Z
