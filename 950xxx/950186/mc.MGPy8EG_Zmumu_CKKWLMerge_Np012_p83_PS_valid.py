
#### Shower 
evgenConfig.description = 'MadGraph_Zmumu'
evgenConfig.keywords+=['Z','jets']
evgenConfig.contact  = [ "chris.g@cern.ch" ]
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 2

PYTHIA8_nJetMax=1
PYTHIA8_Process='pp>e+e-'
PYTHIA8_Dparameter=0.4
PYTHIA8_TMS=30
PYTHIA8_nQuarksMerge=5
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
