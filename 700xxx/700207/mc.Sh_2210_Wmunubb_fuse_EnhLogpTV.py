include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")
include("Sherpa_i/Fusing_Direct.py")
genSeq.Sherpa_i.Parameters += [ "FUSING_DIRECT_FACTOR=1." ]

evgenConfig.description = "Sherpa W -> munu + bb"
evgenConfig.keywords = ["SM", "W", "muon", "jets", "NLO","4FS" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch","matthew.gignac@cern.ch" ]
evgenConfig.nEventsPerJob = 2000

genSeq.Sherpa_i.RunCard="""                             
(run){
  %scales, tags for scale variations                              
  FSF:=1.; RSF:=1.; QSF:=1.;                                                                                                                           
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};                          
  
  % Speed Setup Increase (HtPrime)
  PP_RS_SCALE VAR{sqr(sqrt(H_T2)-PPerp(p[2])-PPerp(p[3])+MPerp(p[2]+p[3]))/4};
  % Negative Weight reduction 
  NLO_CSS_PSMODE=1

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops
  
  % Fusing directing component for 
  FUSING_DIRECT_FACTOR 1.;
  MASSIVE[5]=1

  % EW corrections setup
  OL_PARAMETERS=ew_scheme 2 ew_renorm_scheme 1
  ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1 EW|LO1|LO2 EW|LO1|LO2|LO3;
  EW_SCHEME=3
  GF=1.166397e-5
  METS_BBAR_MODE=5

}(run)                               

(processes){   
  Process 93 93 -> 13 -14 5 -5;
  Order (*,2); 
  CKKW 100000.0;	
  Associated_Contributions EW|LO1|LO2|LO3 {4};
  Enhance_Observable VAR{log10(PPerp(p[2]+p[3]))}|1|3.3 {4}
  NLO_QCD_Mode MC@NLO; 
  ME_Generator Amegic; 
  RS_ME_Generator Comix; 
  Loop_Generator LOOPGEN;  
  Integration_Error 0.05 {4};
  End process;

  Process 93 93 -> -13 14 5 -5;
  Order (*,2);
  CKKW 100000.0;
  Associated_Contributions EW|LO1|LO2|LO3 {4};
  Enhance_Observable VAR{log10(PPerp(p[2]+p[3]))}|1|3.3 {4}
  NLO_QCD_Mode MC@NLO;
  ME_Generator Amegic;
  RS_ME_Generator Comix;
  Loop_Generator LOOPGEN;
  Integration_Error 0.05 {4};
  End process;
}(processes)   

(selector){ 
  Mass 13 -14 2.0 E_CMS
  Mass -13 14 2.0 E_CMS
}(selector) 
"""

genSeq.Sherpa_i.NCores = 16
genSeq.Sherpa_i.Parameters += [ "OL_PARAMETERS=ew_scheme=2 ew_renorm_scheme=1 write_parameters=1" ]
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]
