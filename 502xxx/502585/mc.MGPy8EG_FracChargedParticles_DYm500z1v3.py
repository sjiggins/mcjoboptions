mass = 500
charge = 1.0/3.0
evgenConfig.nEventsPerJob = 30000
get_FractionChargesControlFile = subprocess.Popen(['get_files', '-jo', 'FractionalChargeParticles.py'])
if get_FractionChargesControlFile.wait():
        print "Could not get hold of FractionalChargeParticles.py, exiting..."
        sys.exit(2)
include ( "FractionalChargeParticles.py" )



