from MadGraphControl.MadGraphUtils import *
#import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
import math, subprocess

#MadGraphControl.MadGraphUtils.
MADGRAPH_PDFSETTING={ 'central_pdf':263400, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets 'pdf_variations':[263400], # pdfs for which all variations (error sets) will be included as weights 'alternative_pdfs':[262400,13202], # pdfs for which only the central set will be included as weights 'scale_variations':[0.5,1.,2.], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated #'use_syst': "False", # use this and comment the previous ones if systematic variations are not needed 
}

nameModel=None
nameZp=None
nameMassParamZp=None
nameWidthParamZp=None
nameMassParamhD=None
nameWidthParamhD=None
nameMassParamN2=None
nameWidthParamN2=None
codeZp=None
mg_proc=None
process=None

if model=='LightVector':
    nameZp='vx'
    nameMassParamZp='MVdark'
    nameWidthParamZp='WVdark'
    nameMassParamN2='MX2'
    nameWidthParamN2='Wchip'
    nameModel=model
    codeZp=56
elif model=='InelasticVectorEFT':
    nameZp='zp'
    nameMassParamZp='MZp'
    nameWidthParamZp='MZp'
    nameMassParamN2='Mn2'
    nameWidthParamN2='Wn2'
    nameModel=model
    codeZp=57
elif model=='darkHiggs':
    nameZp='zp'
    nameMassParamZp='MZp'
    nameWidthParamZp='MZp'
    nameMassParamhD='MHD'
    nameWidthParamhD='WhD'
    nameModel=model
    codeZp=56
else:
    raise RuntimeError("Unknown model.")

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")


if model=='LightVector' or model=='InelasticVectorEFT':
    if fs == "ee": 
        mg_proc=[#"define j = j b b~",
            #"define p = p b b~",
            "generate p p > %s n1 n1, %s > e+ e-"%(nameZp,nameZp)] 
            #"generate p p > %s > e+ e-"%nameZp] 
        process='pp>(%s>e+e-)n1n1'%nameZp
        #process='pp>%s>e+e-'%nameZp
    elif fs == "mumu": 
        mg_proc=[#"define j = j b b~",
            #"define p = p b b~",
            "generate p p > %s n1 n1, %s > mu+ mu-"%(nameZp,nameZp)] 
            #"generate p p > %s > mu+ mu-"%nameZp] 
        process='pp>(%s>mu+mu-)n1n1'%nameZp
        #process='pp>%s>mu+mu-'%nameZp
elif model=='darkHiggs':
    if fs == "ee": 
        mg_proc=[#"define j = j b b~",
            #"define p = p b b~",
            "generate p p > hd %s, hd > n1 n1, %s > e+ e-"%(nameZp,nameZp)]
            #"generate p p > %s > e+ e-"%nameZp]
        process='pp>(hd>n1n1)(%s>e+e-)'%nameZp
        #process='pp>%s>e+e-'%nameZp
    elif fs == "mumu": 
        mg_proc=[#"define j = j b b~",
            #"define p = p b b~",
            "generate p p > hd %s, hd > n1 n1, %s > mu+ mu-"%(nameZp,nameZp)]
            #"generate p p > %s > mu+ mu-"%nameZp]
        process='pp>(hd>n1n1)(%s>mu+mu-)'%nameZp
        #process='pp>%s>mu+mu-'%nameZp

full_proc = """
import model %s

%s

output -f
"""%(nameModel,'\n'.join(mg_proc))

process_dir = new_process(full_proc)

get_dat_file = subprocess.Popen(['get_files','-jo', 'MadGraph_param_card_%s_lep.dat'%nameModel])
get_dat_file.wait()

my_settings = {'nevents':int(runArgs.maxEvents*2./filteff),'xqcut':0, 'python_seed':runArgs.randomSeed, 'ebeam1':beamEnergy, 'ebeam2':beamEnergy, 'bwcutoff':250.0}

modify_run_card(process_dir=process_dir, settings=my_settings)

if model=='LightVector' or model=='InelasticVectorEFT':
    params = {'MASS':{'1000022':mDM1,'1000023':mDM2, str(codeZp):mZp}, 'DECAY':{str(codeZp):'DECAY %d %s # W%s'%(codeZp,'Auto',nameWidthParamZp), '1000023':'DECAY %d %s # W%s'%(1000023,'Auto',nameWidthParamN2)}}
    modify_param_card(param_card_input='MadGraph_param_card_%s_lep.dat'%nameModel, process_dir=process_dir, params=params)
elif model=='darkHiggs':
    params = {'MASS':{'1000022':mDM1,str(codeZp):mZp,'26':mHD}, 'DECAY':{str(codeZp):'DECAY %d %s # W%s'%(codeZp, 'Auto', nameWidthParamZp), '26':'DECAY %d %s # W%s'%(26, 'Auto', nameWidthParamhD)}}
    modify_param_card(param_card_input='MadGraph_param_card_%s_lep.dat'%nameModel, process_dir=process_dir, params=params)

print_cards()

runName='run_01'
runArgs.inputGeneratorFile='tmp_LHE_events'
generate(runArgs=runArgs, process_dir=process_dir)
arrange_output(process_dir=process_dir, runArgs=runArgs) 

evgenConfig.description = "Mono Zprime sample - model %s"%nameModel
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.process = process
#evgenConfig.inputfilecheck = runName
evgenConfig.contact = ["Even S. Haaland <even.simonsen.haland@cern.ch"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py") 
include("Pythia8_i/Pythia8_MadGraph.py")

genSeq.Pythia8.Commands += ["1000022:all = chid chid~ 2 0 0 %d 0" %(mDM1),
                            "1000022:isVisible = false"]

include("GeneratorFilters/MissingEtFilter.py")
filtSeq.MissingEtFilter.METCut = 50*GeV
